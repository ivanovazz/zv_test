<?php
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Задание 2</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="form-pos">
                    <form action="index.php" method="post" name="form" id="form">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Введите email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="phone">Номер телефона (пример:+380501111111)</label>
                            <input type="text" class="form-control" id="phone" placeholder="Введите телефон" name="phone">
                        </div>
                        <button type="button" class="btn btn-primary" id="btn">Submit</button>
                    </form>
                    <div id="for-response"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.slim.min.js"></script>
<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/popper.js/dist/umd/popper.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">

        $("#btn").click(function () {
             var form_data = {email: $( '[name="email"]' ).val(), phone: $( '[name="phone"]' ).val()};
            $.ajax({
                type: "POST",
                url: '/script.php',
                data: {form: form_data},
                dataType: 'json',
                success: function (response) {
                    if(response.result){
                        $('#for-response').text("Все правильно");
                        document.form.reset();
                    }else {
                        $('#for-response').text("Не правильый email или телефон");
                    }
                },
                error: function () {
                    $('#for-response').text("error");
                }
            });
        });

</script>
</body>
</html>