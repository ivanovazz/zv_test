var wrap = document.getElementById("wrap");

function widthSlide(wrap) {
    var widthWrap = wrap.offsetWidth;
    var widthImg;
    if (widthWrap>930) {
        widthImg = widthWrap/3;
    } else if (widthWrap<=690 && widthWrap>510) {
        widthImg = widthWrap/2;
    } else if (widthWrap<=510) {
        widthImg = widthWrap;
    }

    return widthImg;
}

window.addEventListener('resize', function(event){
    var width = widthSlide(wrap);
    widthImg(width);

    var i = wrap.getAttribute('i');
    var ul = document.getElementById("slider");
    var move = i*(width+4);
    ul.style.marginLeft = move+"px";
});



function widthImg(widthImg) {
    var parent = document.getElementById("slider");
    var allImg = parent.children;
    for(var j=0; j<allImg.length; j++){
        allImg[j].style.width = widthImg+'px';
    }
    return allImg;
}


function initSlider() {
    var i = 0;
    var width = widthSlide(wrap);
    var count = widthImg(width);
    if(i==0){
        document.getElementById("prev").style.display = "none";
        document.getElementById("next").style.display = "inline-block";
    }
    $('.next').click(function () {

        var width = widthSlide(wrap);
        if( isNaN(i) && i!=0){
            i=0;
        } else {
            i--;
        }
        var ul = document.getElementById("slider");
        var move = i*(width+4);
        ul.style.marginLeft = move+"px";
        wrap.setAttribute('i', i);
        if(count.length == 1-i){
            document.getElementById("next").style.display = "none";
            document.getElementById("prev").style.display = "inline-block";
        }else if(i==0){
            document.getElementById("prev").style.display = "none";
            document.getElementById("next").style.display = "inline-block";
        }else {
            document.getElementById("prev").style.display = "inline-block";
            document.getElementById("next").style.display = "inline-block";
        }
    });
    $('.prev').click(function () {
        var width = widthSlide(wrap);

        if( isNaN(i) && i!=0){
            console.log(i);
        }else{

            //i = i+(width+4);
            i++;
        }
        var ul = document.getElementById("slider");
        var move = i*(width+4);
        ul.style.marginLeft = move+"px";
        wrap.setAttribute('i', -i);
        if(count == -i){
            document.getElementById("next").style.display = "none";
            document.getElementById("prev").style.display = "inline-block";
        }else if(i==0){
            document.getElementById("prev").style.display = "none";
            document.getElementById("next").style.display = "inline-block";
        }else {
            document.getElementById("prev").style.display = "inline-block";
            document.getElementById("next").style.display = "inline-block";
        }
    });
}






$(document).ready(function () {
    initSlider();
});