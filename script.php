<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 13.09.17
 * Time: 14:04
 */


if($_SERVER['REQUEST_METHOD'] == 'POST'){
    spl_autoload_register(function ($class_name) {
        include 'classes/'.$class_name.'.php';
    });
    $email = new Email();
    $phone = new Phone();
    if( isset( $_POST['form'] ) ){
        $req = false;
        $email_name = $_POST['form']['email'];
        $phone_number = $_POST['form']['phone'];
        $validate = $email->validate($email_name);
        $validate_number = $phone->validate($phone_number);
        if (!$validate || !$validate_number) {
            $req = false;
        } else {
            $req = true;
        }
        echo json_encode(['result' => $req]);
        exit;
    }

}
