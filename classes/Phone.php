<?php

/**
 * Created by PhpStorm.
 * User: julia
 * Date: 13.09.17
 * Time: 12:45
 */
class Phone implements Validate
{

    public function validate($phone){
        if(!preg_match('/^\+380[1-9][\d]{8}$/', trim($phone))) {
            return false;
        } else {
            return true;
        }
    }
}