<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 13.09.17
 * Time: 13:09
 */
interface Validate
{
    public function validate($var);
}